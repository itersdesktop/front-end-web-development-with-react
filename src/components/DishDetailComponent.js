import React, { createRef, Component } from "react";
import {
    Breadcrumb, BreadcrumbItem,
    Card, CardBody, CardImg, CardText, CardTitle, Col, Label, Button,
    Modal, ModalBody, ModalHeader, Row
} from "reactstrap";
import moment from "moment";
import { Link } from 'react-router-dom';
import {LocalForm, Control, Errors} from "react-redux-form";
import { Loading } from "./LoadingComponent";
import { baseUrl } from "../shared/baseUrl";
import { FadeTransform, Fade, Stagger } from 'react-animation-components';


function RenderDish({dish}) {
    if (dish !== null) {
        return (
            <FadeTransform in
                           transformProps={{
                               exitTransform: 'scale(0.5) translateY(-50%)'
                           }}>
                <Card>
                    <CardImg width="100%" src={baseUrl + dish.image} alt={dish.name} />
                    <CardBody>
                        <CardTitle>{dish.name}</CardTitle>
                        <CardText>{dish.description}</CardText>
                    </CardBody>
                </Card>
            </FadeTransform>
        );
    } else {
        return (
            <div></div>
        );
    }
}

function RenderComments({comments, postComment, dishId}) {
    if (comments !== null) {
        // const renderedComments = dish.comments.map((cmt) => {
        //     return (
        //         <div key={cmt.id}>
        //             <p>{cmt.comment}<br/>{cmt.author}, {cmt.date}</p>
        //         </div>
        //     );
        // });
        const renderedComments =
            <React.Fragment>
                <ul className="list-unstyled">
                    <Stagger in>
                        {comments.map(cmt => {
                            return (
                                <Fade in><li key={cmt.id}>
                                {cmt.comment}<br/>
                                <span className="font-italic">-- {cmt.author}, &nbsp;
                                    {moment(cmt.date).format('MMM DD, YYYY')}
                                    </span>
                            </li></Fade>
                            )
                        })}
                    </Stagger>
                </ul>
            </React.Fragment>

        return(
            <div className="border-info">
                <h4>Comments</h4>
                {renderedComments}
                <CommentForm dishId={dishId} postComment={postComment} />
            </div>
        );
    } else {
        return (
            <span></span>
        );
    }
}

const DishDetail = (props) => {
    if (props.isLoading) {
        return (
            <div className="container">
                <div className="row">
                    <Loading />
                </div>
            </div>
        );
    } else if (props.errMess) {
        return (
            <div className="container">
                <div className="row">
                    <h4>{props.errMess}</h4>
                </div>
            </div>
        );
    } else if (props.dish !== null && props.dish !== undefined) {
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>{props.dish.name}</h3>
                        <hr />
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 col-md-5 m-1">
                        <RenderDish dish={props.dish} />
                    </div>
                    <div className="col-12 col-md-5 m-1">
                        <RenderComments comments={props.comments}
                                        postComment={props.postComment}
                                        dishId={props.dish.id} />
                    </div>
                </div>
            </div>
        );
    } else {
        return (
            <div className="row"></div>
        );
    }
}

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);

class CommentForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false
        }
        this.toggleCommentModalForm = this.toggleCommentModalForm.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(values) {
        this.toggleCommentModalForm();
        this.props.postComment(this.props.dishId, values.rating, values.author, values.comment);
        console.log('User comments submitted: ' + JSON.stringify(values));
        //alert('User comments submitted: ' + JSON.stringify(values));
    }

    toggleCommentModalForm() {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    }

    componentDidMount() {
        const node = this.wrapper.current;
        /* Uses DOM node  */
    }

    wrapper = createRef();

    render() {
        return (
        <React.Fragment>
            <Button outline onClick={this.toggleCommentModalForm}>
                <i className="fa fa-pencil" />&nbsp;Submit Comments</Button>
            <Modal isOpen={this.state.isModalOpen} toggle={this.toggleCommentModalForm}>
                <ModalHeader toggle={this.toggleCommentModalForm}>Submit Comment</ModalHeader>
                <ModalBody>
                    <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                        <Row className="form-group">
                            <Label htmlFor="rating" md={12}>Rating</Label>
                            <Col md={12}>
                                <Control.select model=".rating" name="rating" className="form-control">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </Control.select>
                            </Col>
                        </Row>
                        <Row className="form-group">
                            <Label htmlFor="author" md={12}>Your Name</Label>
                            <Col md={12}>
                                <Control.text
                                    model=".author" id="author" name="author"
                                    placeholder="Please enter your name" className="form-control"
                                    validators={{
                                        required,
                                        minLength: minLength(3),
                                        maxLength: maxLength(15)
                                    }}
                                />
                                <Errors
                                    className="text-danger" model=".author" show="touched"
                                    messages={{
                                        required: 'Required',
                                        minLength: 'Must be greater than 2 characters',
                                        maxLength: 'Must be 15 characters or less'
                                    }}/>
                            </Col>
                        </Row>
                        <Row className="form-group">
                            <Label htmlFor="comment" md={12}>Your Comments</Label>
                            <Col md={12}>
                                <Control.textarea
                                    model=".comment" id="comment" name="comment" rows="6"
                                    placeholder="Please enter your comments into this box" className="form-control" />
                            </Col>
                        </Row>
                        <Row className="form-group">
                            <Col md={{size: 10, offset: 0}}>
                                <Button type="submit" color="primary">
                                    Submit
                                </Button>
                            </Col>
                        </Row>
                    </LocalForm>
                </ModalBody>
            </Modal>
        </React.Fragment>);
    }
}

export default DishDetail;
